import {Component, OnInit, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {NgForm} from "@angular/forms";
import {Reservation} from "../reservation.model";
import {ReservationService} from "../reservation.service";
import {Room} from "../../rooms/room.model";
import {RoomService} from "../../rooms/room.service";
import {User} from "../../users/user.model";
import {ValidateService} from "../../extra services/validate.service";
import {FlashMessagesService} from "angular2-flash-messages";
import {AuthService} from "../../extra services/auth.service";

@Component({
  selector: 'reservation-input',
  templateUrl: './reservation-input.html'
})
export class ReservationInputComponent implements OnInit {
  @Input() reservations: Reservation[];
  reservation: Reservation = new Reservation(null,null,null,null,null,null,null);
  user: User = new User(null,null,null,null,null,null,null,null,null,null,null);
  availableRooms: Room[] = [];
  rooms: Room[] = [];
  constructor(private reservationService: ReservationService, private roomService : RoomService, private validateService: ValidateService, private authService : AuthService, private flashMessage : FlashMessagesService) { }

  ngOnInit() {
    /*this.reservationService.getReservations()
      .subscribe(data=>{
        for (let i = 0; i < data.length; i++){
          if (data[i].status.lastIndexOf('pending') ==-1 || data[i].status.lastIndexOf('accepted') == -1){
            if (this.roomStrings.lastIndexOf(data[i].rooms) ==-1){
              this.roomStrings.push(data[i].rooms);
            }
          }
        }
    });*/
    if (this.authService.loggedIn() != null){
      this.user = JSON.parse(this.authService.loggedIn());
    }
      this.roomService.getRooms()
        .subscribe(data=>{
          this.rooms= data;
        });
    console.log(this.reservations);
    console.log(this.rooms);
  }

  onSubmit(form: NgForm) {
      let status: string[] = ["pending"];
      let rooms: string[] = [];
      let check: boolean[] = [];
      rooms.push(form.value.selectbox);
      check.push(this.validateDate(form.value.startDate));
      check.push(this.validateDate(form.value.endDate));
      check.push(this.validateTime(form.value.startTime));
      check.push(this.validateTime(form.value.endTime));
      if (check.lastIndexOf(false) == -1 && this.user != null){
        const startDate = this.convertToDate(form.value.startDate, form.value.startTime);
        const endDate = this.convertToDate(form.value.endDate, form.value.endTime);
        const reservation = new Reservation(null, this.user[0]._id, endDate, startDate,form.value.message, status, rooms);
        this.reservationService.newReservation(reservation)
         .subscribe(
         data => console.log(data),
         error => console.log(error)
         );
      }
  }
  validateDate(date){
    if(!this.validateService.validateDate(date)){
      this.flashMessage.show('vul een juiste datum in', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }
    return true;
  }
  validateTime(time){
    if(!this.validateService.validateTime(time)){
      this.flashMessage.show('vul een juist tijdstip in', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }
    return true;
  }
  convertToDate(dateString, timeString){
    let dateArr = dateString.split("-");
    const timeArr = timeString.split(':');
    console.log(dateArr);
    console.log(timeArr);
    const d : Date = new Date(dateArr[2], dateArr[1]-1, dateArr[0]);
    d.setHours(timeArr[0]+1);
    d.setMinutes(timeArr[1]);
    return d;
  }
}
