/**
 * Created by Delt on 24/11/2017.
 */

import 'rxjs/Rx';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Room} from "../rooms/room.model";
import {Reservation} from "./reservation.model";
import {User} from "../users/user.model";
import {Headers} from "@angular/http";

@Injectable()
export class ReservationService{
  constructor (private http: HttpClient){}
  getReservations() : Observable<Array<Reservation>>{
    return this.http.get<Array<Reservation>>('http://localhost:3000/reservations');
  }


  // getRoomsFromReservation(roomIds: string[]) : Observable<Array<Room>>{
  //   let rooms: Room[] = [];
  //   for (let i = 0; i < roomIds.length; i++){
  //     rooms.push(this.http.get<Room>('http://localhost:3000/rooms/' + roomIds[i]));
  //   }
  //   return rooms;
  // }


  getRoomsFromReservation(roomIds: string[]) : Array<Room>{
    console.log(roomIds);
    let rooms: Room[] = [];
    for (let i = 0; i < roomIds.length; i++){
      this.http.get<Room>('http://localhost:3000/rooms/' + roomIds[i])
        .subscribe(
          room => rooms.push(room)
        );
    }
    return rooms;
  }


  getReservationsFromUserid(userId: string) : Array<Reservation>{
    let reservations: Reservation[] = [];
      this.http.get<Reservation>('http://localhost:3000/reservations/user/' + userId)
        .subscribe(
          reservation => reservations.push(reservation)
        );
    return reservations;
  }



  getUserFromReservation(userId: string) : Observable<User>{
    return this.http.get<User>('http://localhost:3000/users/' + userId);
  }

  newReservation(reservation: Reservation){
    return this.http.post<Reservation>('http://localhost:3000/reservations', reservation);
  }

  changeStatus(reservation: Reservation, status: string) {
    reservation.status = [status];
    return this.http.post<Reservation>('http://localhost:3000/reservations/' + reservation._id + '/' + status, reservation);
  }

  // findByName(voornaam: string, achternaam: string) : Observable<User>{
  //
  // }

  getReservationsByRoomId(roomId: string): Observable<Reservation>{
    return this.http.get<Reservation>('http://localhost:3000/reservations/room/'+ roomId);
  }

}
