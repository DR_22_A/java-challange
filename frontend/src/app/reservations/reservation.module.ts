import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReservationsList} from './reservations.list';
import {FormsModule} from "@angular/forms";
import {ReservationService} from "./reservation.service";
import {ReservationComponent} from "./reservation/reservation.component";
import {ReservationInputComponent} from "./new-reservation/reservation-input.component";
import {ReservationsListByRoom} from "./resevations.list.room";

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [ReservationsList, ReservationComponent, ReservationInputComponent, ReservationsListByRoom],
  providers: [ReservationService]
})
export class ReservationModule { }
