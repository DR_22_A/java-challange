/**
 * Created by Delt on 23/11/2017.
 */
import {Component, NgModule} from "@angular/core";
import {ReservationService} from "./reservation.service";
import {Room} from "../rooms/room.model";
import {Reservation} from "./reservation.model";
import {User} from "../users/user.model";
import {RoomService} from "../rooms/room.service";
import {AuthService} from "../extra services/auth.service";
import {Router, ActivatedRoute} from "@angular/router";
@Component({
  selector:'app-reservation-list',
  templateUrl: './reservations.list.html',
})
export class ReservationsList{
  constructor (private reservationService: ReservationService, public authService: AuthService, public route: ActivatedRoute){}
  reservations : Reservation[] = [];
  user: User;
  roomId: string;
  sub: any;
  ngOnInit(){
    this.sub = this.route.params.subscribe(params => {
      this.roomId = params['roomId'];
      console.log(this.roomId);
    });
    this.user = this.authService.loggedIn();
    this.reservationService.getReservations()
      .subscribe(data => {
        this.reservations = data;
      })
  }
}
