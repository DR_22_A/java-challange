import {Component, Input, OnInit} from '@angular/core';
import {Reservation} from "../reservation.model";
import {ReservationService} from "../reservation.service";
import {User} from "../../users/user.model";
import {Room} from "../../rooms/room.model";
import {AuthService} from "../../extra services/auth.service";


@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html'
})
export class ReservationComponent implements OnInit {
  @Input() reservation: Reservation;
  user: User = new User(null,null,null,null,null,null,null,null,null,null,null); //initialisatie user, foutvermijding.
  rooms : Room[] = [];
  startDate : Date;
  endDate : Date;
  email: string;
  constructor(private reservationService: ReservationService, public authService: AuthService) { }
  ngOnInit() {
    this.startDate = new Date(this.reservation.startTime);
    this.endDate = new Date(this.reservation.stopTime);
    this.reservationService.getUserFromReservation(this.reservation.user)  //Werkende, maar IDs in database kunnen verschillen.
      .subscribe(
        data =>{
          this.user = Reservation.getFullUser(data);
        }
      );
    this.rooms = this.reservationService.getRoomsFromReservation(this.reservation.rooms);
    console.log(this.rooms)
  }
  onChangeStatus(reservation: Reservation, status){
    this.reservationService.changeStatus(reservation, status)
      .subscribe(
        data => console.log(data),
        error => console.log(error)
      )
  }
}
