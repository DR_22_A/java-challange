import {User} from "../users/user.model";
import {Room} from "../rooms/room.model";
/**
 * Created by Delt on 24/11/2017.
 */
export class Reservation{

  constructor(public _id : string, public  user: string, public stopTime: Date, public startTime:Date, public message: string, public status: string[], public rooms: string[]){
  }
  public static getFullUser(user: User): User{
    return user; //geeft een functioneel user-object terug
  }
  public static getFullRooms(rooms: Room[]): Room[]{
    return rooms; //geeft een functioneel user-object terug
  }
}
