import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomComponent } from './room/room.component';
import {RoomsComponent} from './rooms.component';
import {EditRoomComponent} from './edit-room/edit-room.component';
import {FormsModule} from "@angular/forms";
import {RoomService} from "./room.service";
import {ConfirmDeleteComponent} from "./confirm-delete/confirm-delete.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [RoomsComponent, RoomComponent, EditRoomComponent, ConfirmDeleteComponent],
  providers: [RoomService],
  entryComponents: [EditRoomComponent,ConfirmDeleteComponent]
})
export class RoomsModule { }
