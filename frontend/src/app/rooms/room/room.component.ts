import {Component, Input, OnInit} from '@angular/core';
import {Room} from '../room.model';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {EditRoomComponent} from "../edit-room/edit-room.component";
import {ConfirmDeleteComponent} from "../confirm-delete/confirm-delete.component";
import {Router} from "@angular/router";
import {AuthService} from "../../extra services/auth.service";

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html'
})
export class RoomComponent implements OnInit {
  @Input() room: Room;
  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService, private router: Router, public authService: AuthService) { }

  ngOnInit() {
  }

  onEdit(){
    this.bsModalRef = this.modalService.show(EditRoomComponent);
    this.bsModalRef.content.title = "Bewerk zaal";
    this.bsModalRef.content.room = this.room;
  }

  onDelete(){
    this.bsModalRef = this.modalService.show(ConfirmDeleteComponent);
    this.bsModalRef.content.room = this.room;
  }

  reservatie(){
    this.router.navigate(['/reservaties', this.room._id]);
  }

}
