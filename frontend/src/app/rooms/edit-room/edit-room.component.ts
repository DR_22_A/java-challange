import { Component } from '@angular/core';
import {RoomService} from '../room.service';
import {Room} from '../room.model';
import {BsModalRef} from "ngx-bootstrap";
import {NgForm} from "@angular/forms";
import {AuthService} from "../../extra services/auth.service";

@Component({
  selector: 'app-edit-room',
  templateUrl: './edit-room.component.html'
})
export class EditRoomComponent {
  title: string;
  room: Room;
  constructor(private roomService: RoomService, public bsModalRef: BsModalRef, public authService: AuthService) { }

  onSubmit(form: NgForm) {
    if (this.room) {
      //edit
      this.room.name = form.value.name;
      this.room.description = form.value.description;
      this.room.url = form.value.url;

      this.roomService.updateRoom(this.room);
      this.room = null;
    }
    else {
      const room = new Room(form.value.name, form.value.description, form.value.url);
      this.roomService.addRoom(room);
    }
    this.onClear(form);
  }

  onClear(form: NgForm){
    this.room = null;
    form.resetForm();
    this.bsModalRef.hide();
  }
}
