export class Room{
  constructor( public name: string, public description: string, public url:string, public _id?: string){ }
}
