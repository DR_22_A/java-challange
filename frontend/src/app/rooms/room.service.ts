import {EventEmitter, Injectable} from "@angular/core";
import 'rxjs/Rx';
import {Room} from './room.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/Observable";

@Injectable()
export class RoomService {
  private readonly URL = 'http://localhost:3000/rooms';
  rooms: Room[]=[];
  roomIsDeleted = new EventEmitter<Room>();
  roomIsAdded = new EventEmitter<Room>();
  constructor (private http: HttpClient){}

  getRooms() : Observable<Array<Room>>{
    let rooms = this.http.get<Array<Room>>(this.URL);

    rooms.subscribe(data => {
      data.forEach(room => { if (!room.name){room.name = ""}});
      this.rooms=data;
    }
    );

    return rooms;
  }

  searchRooms(name: string) : Array<Room>{
    return this.rooms.filter(room => room.name.search(name) >=0 );
  }

  addRoom(room:Room) {
    this.http.post<Room>(this.URL, room).subscribe(
      data => this.addedRoom(data),
      error => console.log(error)
    );
  }

  addedRoom(room:Room){
    this.rooms.push(room);
    this.roomIsAdded.emit(room);
  }

  updateRoom(room: Room) : Observable<Room>{
    return this.http.patch<Room>(`${this.URL}/${room._id}`,room);
  }

  deleteRoom(room: Room)  {
    this.http.delete<Room>(`${this.URL}/${room._id}`).subscribe(
      data => this.deletedRoom(room),
      error => console.log(error)
    );
  }

  deletedRoom(room:Room){
    let index = this.rooms.findIndex(key => key._id == room._id);
    if (index != -1){
      this.rooms.splice(index, 1);
    }
    this.roomIsDeleted.emit(room);
  }
}
