import { Component, OnInit } from '@angular/core';
import {RoomService} from './room.service';
import {Room} from './room.model';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {EditRoomComponent} from "./edit-room/edit-room.component";
import {AuthService} from "../extra services/auth.service";

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html'
})
export class RoomsComponent implements OnInit {
  search: string = "";
  rooms: Room[];
  bsModalRef: BsModalRef;


  constructor(private roomService: RoomService,private modalService: BsModalService, public authService: AuthService) { }

  ngOnInit() {
    this.roomService.getRooms().subscribe(
      data => this.rooms = this.roomService.rooms
    );

    this.roomService.roomIsAdded.subscribe(data => this.onSearch());

    this.roomService.roomIsDeleted.subscribe(this.deletedRoom);

    this.search = "";
  }

  onAdd(){
    this.bsModalRef = this.modalService.show(EditRoomComponent);
    this.bsModalRef.content.title = "Nieuwe zaal";
  }

  onSearch(){
    if (this.search == ""){
      this.rooms = this.roomService.rooms;

    } else {
      this.rooms = this.roomService.searchRooms(this.search);
    }
  }

  deletedRoom(room:Room){
    let index = this.rooms.findIndex(key => key._id == room._id);
    if (index != -1){
      this.rooms.splice(index, 1);
    }
  }

  isAdmin(){
    if (this.authService.loggedIn()){
      return this.authService.isAdmin();
    }
    return false;
  }
}
