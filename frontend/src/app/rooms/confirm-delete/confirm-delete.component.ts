import {Component} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {Room} from "../room.model";
import {RoomService} from "../room.service";

@Component({
  selector: 'app-confirm-delete',
  templateUrl: '../confirm-delete/confirm-delete.component.html'
})
export class ConfirmDeleteComponent  {
  room: Room;

  constructor( public bsModalRef: BsModalRef, private roomService: RoomService) { }

  onConfirm(){
    this.roomService.deleteRoom(this.room);
    this.onClose();
  }

  onClose(){
    this.bsModalRef.hide();
  }

}
