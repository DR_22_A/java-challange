import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollapseModule, ModalModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot()
  ],
  exports: [
    ModalModule,
    CollapseModule
  ],
  declarations: []
})
export class NgxModule {
}
