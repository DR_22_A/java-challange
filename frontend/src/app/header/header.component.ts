import { Component, OnInit } from '@angular/core';
import { AuthService } from "../extra services/auth.service";
import { FlashMessagesService } from "angular2-flash-messages";
import { Router } from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: ['a:hover { cursor: pointer; }']
})
export class HeaderComponent implements OnInit {

  navCollaps = true; // true -> zichtbaar, false -> verberg
  email: string;

  constructor(private authService: AuthService, private router: Router, private flashMessageService: FlashMessagesService) {



  }

  private changeEmail(email: string): void {
    this.email = email;
  }

  ngOnInit() {

    this.authService.getLoggedInName.subscribe(name => this.changeEmail(name));

    if(this.authService.loggedIn()){
      const user = JSON.parse(localStorage.getItem('user'));
      this.email = user[0].email;
    }
  }

  onLogoutClick(){
    this.authService.logout();
    this.flashMessageService.show('U bent uitgelogd', {cssClass: 'alert-success', timeout: 3000});
    this.router.navigate(['login']);
  }

}
