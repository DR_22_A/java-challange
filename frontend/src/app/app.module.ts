import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgxModule} from './ngx.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import {RoomsModule} from './rooms/rooms.module';
import {routing} from "./app.routing";
import {HttpClientModule} from "@angular/common/http";
import {LoginComponent} from "./login/login.component";
import { FormsModule } from "@angular/forms";
import {ValidateService} from "./extra services/validate.service";
import {FlashMessagesModule} from "angular2-flash-messages";
import { AuthService } from "./extra services/auth.service";
import {ReservationModule} from "./reservations/reservation.module";

import {AuthGuard} from "./guards/auth.guard";
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    NgxModule,
    routing,
    HttpClientModule,
    RoomsModule,
    ReservationModule,
    FlashMessagesModule,
    FormsModule
  ],
  providers: [ValidateService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
