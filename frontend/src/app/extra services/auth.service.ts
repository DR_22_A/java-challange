import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/Observable";
import {EventEmitter, Injectable, Output} from "@angular/core";
import 'rxjs/Rx';
import {User} from '../users/user.model';

@Injectable()
export class AuthService {
  @Output() getLoggedInName: EventEmitter<any> = new EventEmitter();

  private readonly URL = 'http://localhost:3000/users';
  authToken: any;
  user: User;

  constructor(private http: HttpClient) { }


  // authenticate(user){
  //   let headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   http://localhost:3000/users/?email=administrator@hotmail.com
  //   return this.http.get()
  // }

  getUser(user){
       return this.http.get(`${this.URL}`, {params : {email: user.email}});
  }


  storeUserData(user){
    console.log('dit is user = ' + user);
    localStorage.setItem('user', JSON.stringify(user));
    const usertje = JSON.parse(localStorage.getItem('user'));
    this.getLoggedInName.emit(usertje[0].email);
  }


  getStoredUserId(){
    const usertje = JSON.parse(localStorage.getItem('user'));
    return usertje[0]._id;
  }

  loggedIn(){
    return localStorage.getItem('user');
  }


  logout(){
    localStorage.clear();
    return true;
  }

  isAdmin(){
    const usertje = JSON.parse(localStorage.getItem('user'));
    if (usertje == null){
      return false;
    }
    if(usertje[0].admin){
      return true;
    }
    else{
      return false
    }
  }

}
