import {Component, OnInit} from '@angular/core';
import { ValidateService } from '../extra services/validate.service';
import {Router} from "@angular/router";

import { User } from "../users/user.model";

import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from "../extra services/auth.service";



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {



  usertje: User;

  email: string;
  password: string;

  constructor(private validateService: ValidateService, private flashMessage : FlashMessagesService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    localStorage.clear();

  }

  onLoginSubmit(){
    const user = {
      email: this.email,
      password: this.password
    }

    //console.log(user);


    //alle velden
    if(!this.validateService.validateEmail(this.email)){
     this.flashMessage.show('vul een juist email in', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }


    //juist email
    if(!this.validateService.validateLogin(user)){
      this.flashMessage.show('vul alle velden in', {cssClass: 'alert-danger', timeout: 3000});
    }



    this.usertje = this.authService.getUser(user).subscribe(data => {
      if(data){
        const localUser = this.usertje
        if(user != null && data != "" && user.password == data[0].password){
          //console.log(user.password + " = user.password");
          //console.log(data + " = data[0].password");

          //if(user.password == data[0].password){
          console.log(data + " = de data");
          this.authService.storeUserData(data);

          this.router.navigate(['reservaties']);
          this.flashMessage.show('U bent ingelogd', {cssClass: 'alert-success', timeout: 3000});
          //}
          // else{
          //   this.flashMessage.show('email en paswoord komen niet overeen', {cssClass: 'alert-danger', timeout: 3000});
          //   localStorage.clear();
          // }
        }
        else{
          this.flashMessage.show('email en paswoord komen niet overeen', {cssClass: 'alert-danger', timeout: 3000});
          localStorage.clear();
        }

      }
    });







  }

}
