import { Component, OnInit } from '@angular/core';
import { ReservationService } from "../reservations/reservation.service";
import {Reservation} from "../reservations/reservation.model";
import { AuthService} from "../extra services/auth.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  reservations : Reservation[] = [];

  constructor(private reservationService: ReservationService, private authService: AuthService) { }

  ngOnInit() {

    // this.reservationService.getUserFromReservation("5a17d5f4dbe05c3f9407b775")  //Werkende, maar IDs in database kunnen verschillen.
    //   .subscribe(
    //     data =>{
    //       this.user = Reservation.getFullUser(data);
    //     }
    //   );

    //this.reservationService.getReservationsFromUserid()

    console.log(this.authService.getStoredUserId());
    //"5a16d8a21c50a73985967ff5"
    this.reservations = this.reservationService.getReservationsFromUserid(this.authService.getStoredUserId());
    console.log(this.reservations);


  }



}
