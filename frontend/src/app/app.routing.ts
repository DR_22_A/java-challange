import {RoomsComponent} from './rooms/rooms.component';
import {Routes, RouterModule} from '@angular/router';
import { ReservationsList} from "./reservations/reservations.list";
import {LoginComponent} from "./login/login.component";
import {ProfileComponent} from "./profile/profile.component";
import { HeaderComponent} from "./header/header.component";

import {AuthGuard} from "./guards/auth.guard";
import {ReservationsListByRoom} from "./reservations/resevations.list.room";

const route: Routes=[
  { path: '', redirectTo: 'reservaties', pathMatch: 'full' },
  {path: 'kamers', component: RoomsComponent},
  {path: 'reservaties', component: ReservationsList},
  {path: 'login', component: LoginComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'reservaties', component: ReservationsList, pathMatch: 'full'},
  {path: 'reservaties/:roomId', component: ReservationsListByRoom},
  {path: 'login', component: LoginComponent}
];
export const routing = RouterModule.forRoot(route);
