/**
 * Created by Delt on 24/11/2017.
 */

import 'rxjs/Rx';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {Room} from "../rooms/room.model";

@Injectable()
export class CalendarService{
  constructor (private http: HttpClient){}
  findReservations(){
    this.http.get('http://localhost:3000/reservations')

  }
  getRooms() : Observable<Array<Room>>{
    return this.http.get<Array<Room>>('http://localhost:3000/rooms');
  }
}
