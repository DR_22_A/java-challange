/**
 * Created by Delt on 23/11/2017.
 */
import {Component, NgModule} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {HttpHandler} from "@angular/common/http";
import {CalendarService} from "./calendar.service";
import {Room} from "../rooms/room.model";
import {toArray} from "rxjs/operators";
@Component({
  selector:'app-calendar',
  templateUrl: './calendar-component.html',
})
export class CalendarComponent{
  constructor (private calendarService: CalendarService){}
  test : Object;
  rooms : Room[] = [];
  ngOnInit(){
    this.calendarService.getRooms()
      .subscribe(data => {
        this.rooms = data;
      })
  }
  onBtnClick(){
    for (let room of this.rooms){
      console.log(room.name);
    }
  }
  onBtnLog(){

  }
}
