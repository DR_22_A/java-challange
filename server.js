/**
 * Created by Eigenaar on 23/11/2017.
 */
const express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    cors = require('cors')

    , Room = require('./backend/api/models/roomModel'), //created model loading here
    Reservation = require("./backend/api/models/reservationModel"),
    User = require("./backend/api/models/userModel")
    ;

let app = express(),
    port = process.env.PORT || 3000;

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://administrator:groep8iscool@ds117956.mlab.com:17956/roomreservation');

app.use(cors());

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


const routes = require('./backend/api/routes/roomRoutes'); //importing route
routes(app); //register the route

const reservRoutes = require('./backend/api/routes/reservationRoutes');
reservRoutes(app);

const userRoutes = require('./backend/api/routes/userRoutes');
userRoutes(app);

app.listen(port);

app.use(function (req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

console.log('Room Reservation RESTful API server started on: ' + port);