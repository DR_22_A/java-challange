/**
 * Created by Eigenaar on 23/11/2017.
 */
'use strict';
module.exports = function (app) {
    const reservations = require('../controllers/reservationController');

    app.route('/reservations')
        .get(reservations.list_all_reservations)
        .post(reservations.new_reservation);

    app.route('/reservations/:reservationId')
        .get(reservations.list_single_reservation)
        .put(reservations.update_reservation)
        .delete(reservations.remove_reservation);

    app.route('/reservations/room/:roomId')
        .get(reservations.list_single_reservation_by_roomId);

    app.route('/reservations/user/:userId')
        .get(reservations.list_single_reservation_by_userId);

    app.route('/reservations/:reservationId/:status')
        .post(reservations.update_status)

    app.route('/reservationsbystatus/:status')
        .get(reservations.getByStatus);
};