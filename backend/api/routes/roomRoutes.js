/**
 * Created by Eigenaar on 23/11/2017.
 */
const mongoose = require('mongoose');
mongoose.Promise = Promise;
'use strict';
module.exports = function (app) {
    const rooms = require('../controllers/roomController');

    app.route('/rooms')
        .get(rooms.list_all_rooms)
        .post(rooms.new_room);

    app.route('/rooms/:roomId')
        .get(rooms.list_single_room)
        .patch(rooms.update_room)
        .put(rooms.update_room)
        .delete(rooms.remove_room);

}