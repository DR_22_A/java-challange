/**
 * Created by Dries on 23/11/2017.
 */
const mongoose = require('mongoose');
mongoose.Promise = Promise;
'use strict';
module.exports = function (app) {
    const users = require('../controllers/userController');


    app.route('/users/')
        .get(users.list_all_users)
        .post(users.new_user);

    app.route('/users/:userId')
        .get(users.list_single_user)
        .put(users.update_user)
        .delete(users.remove_user);

}