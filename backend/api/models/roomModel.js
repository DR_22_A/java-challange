/**
 * Created by Eigenaar on 23/11/2017.
 */

'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require('mongoose-type-url');


const RoomSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    tag: {
        type: String
    },
    url: {
        type: mongoose.SchemaTypes.Url
    }
    // type: [{
    //         type: String,
    //         enum: ['feestzaal', 'mediaruimte', 'studio', 'bar/lounge', 'akoestische ruimte', 'vergaderzaal', 'danszaal 1', 'danszaal 2', 'danszaal 3'],
    //         default: [undefined]
    // }]
});

module.exports = mongoose.model('Room', RoomSchema);