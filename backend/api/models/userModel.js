/**
 * Created by Dutchy on 23/11/2017.
 */

'use strict';
const mongoose = require('mongoose'), Schema = mongoose.Schema;

const UserSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    birthday: {
        type: Date
    },
    address: {
        type: String
    },
    postalCode: {
        type: String
    },
    city: {
        type: String
    },
    active: {
        type: Boolean
    },
    admin: {
        type: Boolean
    }
});

module.exports = mongoose.model('User', UserSchema);