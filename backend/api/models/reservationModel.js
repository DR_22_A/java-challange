/**
 * Created by Dutchy on 23/11/2017.
 */

'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const user = require('./userModel');


const ReservationSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId, ref: 'User',
        required: true
    },
    rooms: [{
        type: mongoose.Schema.Types.ObjectId, ref: 'Room',
        required: true
    }],
    message: {
        type: String,
        required: true
    },
    startTime: {
        type: Date,
        required: true
    },
    stopTime: {
        type: Date,
        required: true
    },
    status: [{
        type: String,
        enum: ['pending', 'accepted', 'denied', 'changed', 'cancelled', 'deleted']
    }]
});

module.exports = mongoose.model('Reservation', ReservationSchema);