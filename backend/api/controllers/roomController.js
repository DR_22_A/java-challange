const roomsRepository = require('../repositories/roomRepository');
const usersRepository = require('../repositories/userRepository');

const reservationRepository = require('../repositories/reservationRepository');

exports.new_room = function (req, res) {
    const roomData = req.body;
    roomsRepository.save_new_room(roomData).then(room => {
        res.json(room);
    })
        .catch(err => {
            res.send(err);
        })
};

exports.list_all_rooms = function (req, res) {
    let query = {};
    if (typeof req.query.tag !== 'undefined') {
        // the variable is defined
        query = {
            tag: req.query.tag
        };
    }
    roomsRepository.get_list_all_rooms(query).then(room => {
        res.json(room);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.list_single_room = function (req, res) {

    roomsRepository.get_list_single_room(req.params.roomId).then(room => {
        res.json(room);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.update_room = function (req, res) {

    roomsRepository.save_update_room(req.params.roomId, req.body).then(room => {
        res.json(room);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.remove_room = function (req, res) {

    const query = {rooms: req.params.roomId};
    reservationRepository.get_list_all_reservations(query).then(reservation => {
        if (reservation != undefined || reservation != null || reservation != "") {
            reservation.forEach(reserv => {
                reservationRepository.save_update_reservation(reserv._id, {$push: {"status": "deleted"}}).then(reserv => {
                    //res.json(reserv);
                })
                    .catch(err => {
                        res.send(err);
                    })
            });
        }
        else {
            res.send("Er waren geen reservations bij deze room");
        }
        roomsRepository.save_remove_room(req.params.roomId).then(room => {
            res.json(room);
        })
            .catch(err => {
                return res.send(err);
            });

    })
        .catch(err => {
            res.send(err);
        });
};