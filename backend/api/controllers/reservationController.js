/**
 * Created by Delt on 23/11/2017.
 */
const mongoose = require('mongoose'),
    Reservation = mongoose.model('Reservation');
const reservationRepository = require('../repositories/reservationRepository');
const userRepository = require('../repositories/userRepository');
const roomRepository = require('../repositories/roomRepository');
const nodemailer = require('nodemailer');
const dateFormat = require('dateformat');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'javachallengegroep8@gmail.com',
        pass: 'groep8iscool'
    }
});


exports.createReservation = function (reservationData) {
    return new Promise(function (resolve, reject) {
        if (!reservationData.user || !reservationData.rooms || !reservationData.message || !reservationData.startTime || !reservationData.stopTime) {
            reject('Missing fields');
            return;
        }

        reservationsDB.saveReservation(reservationData)
            .then(reservation => {
                resolve(reservation);
            })
            .catch(err => {
                reject(err);
            })
    });
};

exports.new_reservation = function (req, res) {
    const reservationData = req.body;
    reservationRepository.save_new_reservation(reservationData).then(reservation => {
        res.json(reservation);
    })
        .catch(err => {
            res.send(err);
        })
};

exports.list_all_reservations = function (req, res) {
    let query = {};
    if (typeof req.query.startTime !== 'undefined' && typeof req.query.stopTime !== 'undefined') {
        // the variable is defined
        const dateStart = new Date(req.query.startTime), dateStop = new Date(req.query.stopTime),
            query = {
                $or: [{startTime: {$lte: dateStart}, stopTime: {$gt: dateStart}}, {
                    startTime: {$lt: dateStop},
                    stopTime: {$gte: dateStop}
                }, {startTime: {$gte: dateStart}, stopTime: {$lte: dateStop}}]
            };
        /*reservationRepository.get_list_all_reservations(query).then(reservation => {
         console.log(query);
         res.json(reservation);
         })
         .catch(err => {
         res.send(err);
         });*/
    }
    reservationRepository.get_list_all_reservations(query).then(reservation => {
        res.json(reservation);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.list_single_reservation = function (req, res) {

    reservationRepository.get_list_single_reservation(req.params.reservationId).then(reservation => {
        res.json(reservation);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.list_single_reservation_by_roomId = function (req, res) {

    if (typeof req.query.startTime !== 'undefined' && typeof req.query.stopTime !== 'undefined') {
        // the variable is defined
        const dateStart = new Date(req.query.startTime), dateStop = new Date(req.query.stopTime),
            query = {
                rooms: req.params.roomId,
                $or: [{startTime: {$lte: dateStart}, stopTime: {$gt: dateStart}}, {
                    startTime: {$lt: dateStop},
                    stopTime: {$gte: dateStop}
                }, {startTime: {$gte: dateStart}, stopTime: {$lte: dateStop}}]
            };
        reservationRepository.get_list_all_reservations(query).then(reservation => {
            res.json(reservation);
        })
            .catch(err => {
                res.send(err);
            });
    }
    else {
        query = {rooms: req.params.roomId}
        reservationRepository.get_list_all_reservations(query).then(reservation => {
            res.json(reservation);
        })
            .catch(err => {
                res.send(err);
            });
    }

};

exports.list_single_reservation_by_userId = function (req, res) {

    if (typeof req.query.startTime !== 'undefined' && typeof req.query.stopTime !== 'undefined') {
        // the variable is defined
        const dateStart = new Date(req.query.startTime), dateStop = new Date(req.query.stopTime),
            query = {
                user: req.params.userId,
                $or: [{startTime: {$lte: dateStart}, stopTime: {$gt: dateStart}}, {
                    startTime: {$lt: dateStop},
                    stopTime: {$gte: dateStop}
                }, {startTime: {$gte: dateStart}, stopTime: {$lte: dateStop}}]
            };
        reservationRepository.get_list_all_reservations(query).then(reservation => {
            res.json(reservation);
        })
            .catch(err => {
                res.send(err);
            });
    }
    else {
        query = {user: req.params.userId}
        reservationRepository.get_list_all_reservations(query).then(reservation => {
            res.json(reservation);
        })
            .catch(err => {
                res.send(err);
            });
    }
};

//update status + stuur customized mail naar user die de reservatie gemaakt heeft
exports.update_status = function (req, res, next) {
    const query = {status: req.params.status, _id: req.params.reservationId};
    reservationRepository.get_list_all_reservations(query).then(reservations => {
        if (reservations == undefined || reservations == null || reservations == "") { //check ofdat de reservatie al een bepaalde status heeft
            reservationRepository.save_update_reservation(req.params.reservationId, {$push: {"status": req.params.status}}).then(reservation => {
                if (new Date(reservation.startTime).getTime() > Date.now()) { //check ofdat reservationdate voor huidig tijdstip is
                    userRepository.get_list_single_user(reservation.user).then(user => { //get user
                        roomRepository.get_list_single_room(reservation.rooms).then(room => { //get room

                            let text = "<p>Dag " + user.firstName + "</p><br/><p>De status van je reservatie is gewijzigd naar: ";
                            switch (req.params.status) { //customize text naarmate de status
                                case "accepted":
                                    text += "Aanvaard.</p><p>Bekijk hieronder ter bevestiging de gegevens van je aanvraag.</p>";
                                    break;
                                case "denied":
                                    text += "Geweigerd.</h4><p>Je kan opnieuw proberen een reservatie aan te vragen op onze website of," +
                                        " als er iets niet duidelijk is, contact met ons opnemen.</p>";
                                    break;
                                case "changed":
                                    text += "Aangepast.</h4><p>Bekijk hieronder je reservatie om de aanpassingen te zien.</p>";
                                    break;
                                case "cancelled":
                                    text += "Geannuleerd.</h4><p>Contacteer ons voor meer info over je geannuleerde reservatie.</p>";
                                    break;
                                case "deleted":
                                    text += "Verwijderd.</h4><p>Contacteer ons voor meer info over je verwijderde reservatie.</p>";
                                    break;
                                default:
                                    text += "In afwachting.</h4><p>Ons team bekijkt zo snel je aanvraag voor je reservatie. " +
                                        "We laten je weten wanneer je reservatie is goedgekeurd of indien er iets mis zou zijn met deze reservatie.</p>";
                                    break;
                            } //end switch

                            text += "<p><b>Jouw reservatie:</b></p>" +
                                "<p>Aangevraagd door: " + user.firstName + " " + user.lastName + "</p>" +
                                "<p>Zaal: " + room.name + "</p>" +
                                "<p>Start reservatie: " + dateFormat(reservation.startTime, 'HH:MM dd/mm/yyyy') + "</p>" +
                                "<p>Einde reservatie: " + dateFormat(reservation.stopTime, 'HH:MM dd/mm/yyyy') + "</p>" +
                                "<p>Bericht bij reservatie: " + reservation.message + "</p>" +
                                "<p>Laatste status: " + req.params.status + "</p>" +
                                "<br/><p>Met vriendelijke groeten</p><p>Het LGU-team</p>";

                            let mailOptions = {
                                from: 'javachallengegroep8@gmail.com',
                                to: user.email,
                                subject: 'De status van je reservatie bij "Let\'s Go Urban" is gewijzigd',
                                html: text
                            };

                            transporter.sendMail(mailOptions, function (error, info) {
                                if (error) {
                                    console.log(error);
                                    return res.send(error);
                                } else {
                                    //test output in console://
                                    //console.log('Email sent: ' + info.response);
                                    //console.log('Receiver: ' + mailOptions.to);
                                    //console.log('HTML: ' + mailOptions.html);
                                }
                            });
                        }).catch(err => {
                            return res.send(err);
                        });
                    })
                        .catch(err => {
                            return res.send(err);
                        });
                } //end if
                res.json(reservation);
            })
                .catch(err => {
                    return res.send(err);
                });
        }
        else {
            res.send("Je kan niet twee keer dezelfde status toevoegen!!");
        }
    })
        .catch(err => {
            return res.send(err);
        });

};

exports.getByStatus = function (req, res) {
    const query = {status: req.params.status}
    reservationRepository.get_list_all_reservations(query).then(reservation => {
        res.json(reservation);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.update_reservation = function (req, res) {

    reservationRepository.save_update_reservation(req.params.reservationId, req.body).then(reservation => {
        res.json(reservation);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.remove_reservation = function (req, res) {

    reservationRepository.save_remove_reservation(req.params.reservationId).then(reservation => {
        res.json(reservation);
    })
        .catch(err => {
            res.send(err);
        });
};

