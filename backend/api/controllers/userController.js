const usersRepository = require('../repositories/userRepository');

exports.new_user = function (req, res) {
    const userData = req.body;
    usersRepository.save_new_user(userData).then(user => {
        res.json(user);
    })
        .catch(err => {
            res.send(err);
        })
};

exports.list_all_users = function (req, res) {
    let query = {};
    if (typeof req.query.email !== 'undefined') {
        // the variable is defined
        query = {
            email: req.query.email
        };
    }
    usersRepository.get_list_all_users(query).then(user => {
        res.json(user);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.list_single_user = function (req, res) {

    usersRepository.get_list_single_user(req.params.userId).then(user => {
        res.json(user);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.update_user = function (req, res) {

    usersRepository.save_update_user(req.params.userId, req.body).then(user => {
        res.json(user);
    })
        .catch(err => {
            res.send(err);
        });
};

exports.remove_user = function (req, res) {

    usersRepository.save_remove_user(req.params.userId).then(user => {
        res.json(user);
    })
        .catch(err => {
            res.send(err);
        });
};