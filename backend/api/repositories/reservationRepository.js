const mongoose = require('mongoose'),
    Reservation = mongoose.model('Reservation');

exports.save_new_reservation = function (reservationData) {
    const reservation = new Reservation(reservationData);
    return new Promise(function (resolve, reject) {
        reservation.save()
            .then(reservation => {
                console.log("Reservation saved!");
                resolve(reservation);
            })
            .catch(err => {
                console.log("Error saving reservation: " + err);
                reject(err);
            })
    })
}

exports.get_list_all_reservations = function (query) {
    return new Promise(function (resolve, reject) {
        Reservation.find(query, function (err, reservation) {
            if (err)
                reject(err);
            resolve(reservation);
        })
    })
};

exports.get_list_single_reservation = function (reservationId) {
    return new Promise(function (resolve, reject) {
        Reservation.findById(reservationId, function (err, reservation) {
            if (err)
                reject(err);
            resolve(reservation);
        })
    })
};

exports.save_update_reservation = function (reservationId, reservation) {
    return new Promise(function (resolve, reject) {
        Reservation.findOneAndUpdate({_id: reservationId}, reservation, {new: true}).then(reservation => {
            console.log("Reservation updated!");
            resolve(reservation);
        })
            .catch(err => {
                console.log("Error updating reservation: " + err);
                reject(err);
            })
    })
};

exports.save_remove_reservation = function (reservationId) {
    return new Promise(function (resolve, reject) {
        Reservation.remove({
            _id: reservationId
        }, function (err, reservation) {
            if (err)
                reject(err);
            resolve({message: 'Reservation successfully deleted'});
        });
    })
};
