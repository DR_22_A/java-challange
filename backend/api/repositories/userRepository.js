/**
 * Created by Delt on 23/11/2017.
 */
const mongoose = require('mongoose'),
    User = mongoose.model('User');

exports.save_new_user = function (userData) {
    const user = new User(userData);
    return new Promise(function (resolve, reject) {
        user.save()
            .then(user => {
                console.log("User saved!");
                resolve(user);
            })
            .catch(err => {
                console.log("Error saving user: " + err);
                reject(err);
            })
    })
}

exports.get_list_all_users = function (query) {
    return new Promise(function (resolve, reject) {
        User.find(query).then(user => {
            console.log("User list received.");
            resolve(user);
        })
            .catch(err => {
                console.log("Error getting users: " + err);
                reject(err);
            })
    })
};

exports.get_list_single_user = function (userId) {
    return new Promise(function (resolve, reject) {
        User.findById(userId).then(user => {
            console.log("User received.");
            resolve(user);
        })
            .catch(err => {
                console.log("Error getting user: " + err);
                reject(err);
            })
    })
};

exports.save_update_user = function (userId, user) {
    return new Promise(function (resolve, reject) {
        User.findOneAndUpdate({_id: userId}, user, {new: true}).then(user => {
            console.log("User updated!");
            resolve(user);
        })
            .catch(err => {
                console.log("Error updating user: " + err);
                reject(err);
            })
    })
};

exports.save_remove_user = function (userId) {
    return new Promise(function (resolve, reject) {
        User.remove({_id: userId}).then(
            console.log("User removed."),
            resolve({message: 'User successfully deleted'})
        ).catch(err => {
            console.log("Error removing user: " + err);
            reject(err);
        });
    })
};




