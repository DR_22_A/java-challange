/**
 * Created by Delt on 23/11/2017.
 */
const mongoose = require('mongoose'),
    Room = mongoose.model('Room');

exports.save_new_room = function (roomData) {
    const room = new Room(roomData);
    return new Promise(function (resolve, reject) {
        room.save()
            .then(room => {
                console.log("Room saved!");
                resolve(room);
            })
            .catch(err => {
                console.log("Error saving room: " + err);
                reject(err);
            })
    })
}

exports.get_list_all_rooms = function (query) {
    return new Promise(function (resolve, reject) {
        Room.find(query).then(room => {
            console.log("Room list received.");
            resolve(room);
        })
            .catch(err => {
                console.log("Error getting rooms: " + err);
                reject(err);
            })
    })
};

exports.get_list_single_room = function (roomId) {
    return new Promise(function (resolve, reject) {
        Room.findById(roomId).then(room => {
            console.log("Room received.");
            resolve(room);
        })
            .catch(err => {
                console.log("Error getting room: " + err);
                reject(err);
            })
    })
};

exports.save_update_room = function (roomId, room) {
    return new Promise(function (resolve, reject) {
        Room.findOneAndUpdate({_id: roomId}, room, {new: true}).then(room => {
            console.log("Room updated!");
            resolve(room);
        })
            .catch(err => {
                console.log("Error updating room: " + err);
                reject(err);
            })
    })
};

exports.save_remove_room = function (roomId) {
    return new Promise(function (resolve, reject) {
        Room.remove({_id: roomId}).then(
            console.log("Room removed."),
            resolve({message: 'Room successfully deleted'})
        ).catch(err => {
            console.log("Error removing room: " + err);
            reject(err);
        });
    })
};

/*exports.getRoomsByTag = function (tag) {
 Room.find({tag: tag}, function(err, room){
 if (err)
 res.send(err);
 res.json(room);
 })
 };*/




